//
//  WGViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/29/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import "WGViewController.h"
#import "UserTypeViewController.h"
#import "WebViewController.h"
#import "WGAppDelegate.h"
@interface WGViewController ()

@end

@implementation WGViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  
    [super viewDidLoad];
  
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated
{
 //[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(goToUser) userInfo:nil repeats:NO];

}
-(IBAction)goToWebview:(id)sender
{
    WebViewController *wb=[[WebViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:wb animated:YES];

}
-(IBAction)goToUserType:(id)sender
{
    UserTypeViewController *ut=[[UserTypeViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:ut animated:YES];
}
-(void)goToUser
{
    UserTypeViewController *ut=[[UserTypeViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:ut animated:YES];
}

-(IBAction)dialingButtonClicked:(id)sender
{
//     [self sendPushNotification];
//    WGAppDelegate *app = (WGAppDelegate *)[UIApplication sharedApplication].delegate;
//    [app sendPushNotification];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"ADVERTENCIA" message:@"Esta llamada tendrá un costo de acuerdo a las tarifas vigentes de tu operador, de lo contrario comunícate con el # 324" delegate:self cancelButtonTitle:nil otherButtonTitles:@"ACEPTAR",@"CANCELAR", nil];
    [alertView show];
    
    /*
    NSString * listingPhoneNumber = @"0317460392";
    NSString *phoneCallNum = [NSString stringWithFormat:@"tel:%@",listingPhoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];*/
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        NSString * listingPhoneNumber = @"0317460392";
        NSString *phoneCallNum = [NSString stringWithFormat:@"tel:%@",listingPhoneNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
