//
//  UserTwoViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/30/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import "UserTwoViewController.h"
#import "UserLoginViewController.h"
#import "UserSecondReportViewController.h"
#import "WGAppDelegate.h"
@interface UserTwoViewController ()

@end

@implementation UserTwoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)enter:(id)sender
{
    app.userName = tf3.text;
    app.password = tf4.text;
    [app DismisHud];
    app.userType = [NSNumber numberWithInt:2];
    UserSecondReportViewController *usr=[[UserSecondReportViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:usr animated:YES];
    
}
-(IBAction)userSecondReport: (id)sender
{
    NSString *usernameInDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userType_2_Username"];
    NSString *userPasswordInDefaults =  [[NSUserDefaults standardUserDefaults]objectForKey:@"userType_2_Password"];
    
    if(([tf3.text isEqualToString:usernameInDefaults]) && ([tf4.text isEqualToString:userPasswordInDefaults])){
        UserSecondReportViewController *usr=[[UserSecondReportViewController alloc]initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:usr animated:YES];
    }
        
    
    else{
        
        if(tf3.text.length<1 ||tf4.text.length<1)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops" message:@"Required field" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else{
            app.userType = [NSNumber numberWithInt:2];
            
            app = (WGAppDelegate *)[UIApplication sharedApplication].delegate;
            [app checkUserName:tf3.text :tf4.text :[NSNumber numberWithInt:2]];
            [app ShowHud];
        }

    }
        
        
        
        
    
   

   }

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==tf3)
    {
        
        //UIView* view = [self.view viewWithTag:100];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^
         {
             [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-50, self.view.frame.size.width, self.view.frame.size.height)];
         }
                         completion:^(BOOL finished)
         {
             NSLog(@"Completed");
             
         }];
        
        
    }
    else if (textField==tf4)
    {
        //UIView* view = [self.view viewWithTag:100];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^
         {
             [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-50, self.view.frame.size.width, self.view.frame.size.height)];
         }
                         completion:^(BOOL finished)
         {
             NSLog(@"Completed");
             
         }];
    }
    return YES;
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
    
    
}
-(IBAction)dismisskey:(id)sender
{
    [sender resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    app.userType = [NSNumber numberWithInt:2];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveUserNotification:)
                                                 name:@"UserNameCheck_Notification"
                                               object:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void) receiveUserNotification:(NSNotification *) notification
{
    app.userName = tf3.text;
    app.password = tf4.text;
    [app DismisHud];
    [[NSUserDefaults standardUserDefaults]setObject:app.userName forKey:@"userType_2_Username"];
    [[NSUserDefaults standardUserDefaults]setObject:app.password forKey:@"userType_2_Password"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    app.userType = [NSNumber numberWithInt:2];
    UserSecondReportViewController *usr=[[UserSecondReportViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:usr animated:YES];

}
-(void)viewWillAppear:(BOOL)animated
{
    isPreviouslyLoggedIn = NO;
    if(!([[[NSUserDefaults standardUserDefaults]objectForKey:@"userType_2_Username"] length]<=0))
    {
        tf3.text =[[NSUserDefaults standardUserDefaults]objectForKey:@"userType_2_Username"];
    }
    if(!([[[NSUserDefaults standardUserDefaults]objectForKey:@"userType_2_Password"] length]<=0))
    {
        tf4.text =[[NSUserDefaults standardUserDefaults]objectForKey:@"userType_2_Password"];
    }
    if((tf3.text != nil) && (tf4.text != nil)){
        isPreviouslyLoggedIn = YES;
    }
    app.userType = [NSNumber numberWithInt:2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
