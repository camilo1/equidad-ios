//
//  WebViewController.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/30/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
{
   IBOutlet UIWebView *webView;
}
-(IBAction)goToUserType:(id)sender;

@end
