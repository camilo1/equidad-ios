//
//  main.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/29/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WGAppDelegate class]));
    }
}
