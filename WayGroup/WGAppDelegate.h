//
//  WGAppDelegate.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/29/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//
// Descripción de el hallazgo:
// Ubicación:
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"
#import <Parse/Parse.h>

//com.1tucan.Waygroup
@interface WGAppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>
{

CLLocationManager *locationManager;
CLGeocoder *ceo;
     CLPlacemark *placemark;
    NSString *address;
}
@property (strong) MBProgressHUD *hud;
-(void)ShowHud;
-(void)DismisHud;


@property (strong, nonatomic)  NSMutableArray *imageObjectIDArray;
@property (strong, nonatomic)  NSMutableArray *imageDescriptionArray;
@property (strong, nonatomic)  NSMutableArray *imageLocationArray;
@property (strong, nonatomic)  NSMutableArray *imageDateTimeArray;
@property (strong, nonatomic)  NSMutableArray *imageDataArray;
@property (strong, nonatomic)  NSMutableArray *unloadedImages;
@property (strong, nonatomic)  NSMutableArray *imageTitleArray;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *ownerName;
@property (strong, nonatomic) NSString *deviceID;
@property (strong, nonatomic) NSNumber *userType;

@property   BOOL didRegisterForPNonHeroku;
@property (strong, nonatomic)  NSData *deviceToken;


-(void)getImage:(NSString *)userName;
-(void)checkUserName:(NSString *)userName : (NSString *)password :(NSNumber *)userType;
- (void)uploadImage:(UIImage *)image Description:(NSString *)Des Title:(NSString*)title;

@end
