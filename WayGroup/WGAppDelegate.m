//
//  WGAppDelegate.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/29/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import "WGAppDelegate.h"
#import "WGViewController.h"
#import "Reachability.h"
#import <Parse/Parse.h>

#define ParseApplicationID @"CpkjkPtVi84QARtGotGpJ99Q1XnJgJsvjfEfsqsK"
#define ParseCliendKey @"QTrhSF0HDPPTfw9TMR22oBfmJa1i4jyzFBPi1cGU"


@implementation WGAppDelegate
@synthesize imageDataArray,imageDateTimeArray,imageDescriptionArray,imageLocationArray,imageObjectIDArray,unloadedImages,imageTitleArray;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  //  [self testPost];
    
    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration>  _Nonnull configuration) {
        // Add your Parse applicationId:
        configuration.applicationId = @"CpkjkPtVi84QARtGotGpJ99Q1XnJgJsvjfEfsqsK";
        
        // Uncomment and add your clientKey (it's not required if you are using Parse Server):
        // configuration.clientKey = @"your_client_key";
        
        // Uncomment the following line and change to your Parse Server address;
        configuration.server = @"https://equidad.herokuapp.com/parse/";
        
        // Enable storing and querying data from Local Datastore. Remove this line if you don't want to
        // use Local Datastore features or want to use cachePolicy.
        // configuration.localDatastoreEnabled = YES;
    }]];
    
    //[PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
//    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    self.userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"userName"];
    self.ownerName = [[NSUserDefaults standardUserDefaults] valueForKey:@"ownerName"];
    if(self.userName != nil && self.ownerName!=nil){
        self.unloadedImages = [self getUnloadedImagesOfUser];
        [self uploadUnloadedImages];
    }

    self.imageObjectIDArray = [[NSMutableArray alloc]init];
    self.imageDescriptionArray= [[NSMutableArray alloc]init];
    self.imageLocationArray= [[NSMutableArray alloc]init];
    self.imageDataArray= [[NSMutableArray alloc]init];
    self.imageDateTimeArray= [[NSMutableArray alloc]init];
    self.imageTitleArray = [[NSMutableArray alloc]init];

    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    WGViewController *wg = [[WGViewController alloc] initWithNibName:@"WGViewController"
                                                              bundle:nil];
    UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:wg];
    self.window.rootViewController = navigation;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [navigation setNavigationBarHidden:YES];
    [self.window makeKeyAndVisible];
    
    locationManager = [[CLLocationManager alloc] init];
    ceo = [[CLGeocoder alloc]init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([[UIDevice currentDevice].systemVersion floatValue]>=8.0)
    {
        [locationManager requestAlwaysAuthorization];
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    
    if ([CLLocationManager locationServicesEnabled]) {
        // The location services are available.
        NSLog(@"correct");
    }
    // remote notification------
    
    if([[[UIDevice currentDevice] systemVersion]floatValue] >= 8.0)
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    else
    {
        
        // Register for Push Notitications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    
    //    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    //    NSLog(@"selected language--%@",[NSLocale preferredLanguages]);
    // Override point for customization after application launch.
    return YES;
}

-(void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(nonnull NSError *)error{
    
    NSLog(@"Error:%@",error);
}
// Store the deviceToken in the current installation and save it to Parse.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    self.deviceToken = deviceToken;
    [self registerForPNonHeroku];
    
    // Store the deviceToken in the current installation and save it to Parse.
    // Store the deviceToken in the current installation and save it to Parse.
//    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    [currentInstallation setDeviceTokenFromData:deviceToken];
//    NSString* deviceTokenVal = [[[[deviceToken description]
//                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
//                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
//                                stringByReplacingOccurrencesOfString: @" " withString: @""];
//    
//    
//    NSString *ID = [NSString stringWithFormat:@"user%@",deviceTokenVal];
//    currentInstallation.channels = @[ID];
//    self.deviceID = ID;
//    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        if(succeeded)
//        {
//            //            self.deviceID =currentInstallation.deviceToken;
//            // NSLog(@"%@",self.deviceID);
//        }
//        else{
//            NSLog(@"not Succeeded");
//        }
    }

-(void)registerForPNonHeroku{
    
    if(self.ownerName != NULL) {
        
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        [currentInstallation setDeviceTokenFromData:self.deviceToken];
        NSString* deviceTokenVal = [[[[self.deviceToken description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
        
        
        NSString *ID = [NSString stringWithFormat:@"user%@",deviceTokenVal];
        // currentInstallation.channels = @[ID];
        
        currentInstallation.channels = @[self.ownerName];
        self.deviceID = ID;
        [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(succeeded)
            {
                NSLog(@"Installation successul");
                self.didRegisterForPNonHeroku = true;
            }
            else{
                NSLog(@"not Succeeded");
                self.didRegisterForPNonHeroku = false;
            }
        }];
        
    }else{
        self.didRegisterForPNonHeroku = false;
    }
    
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    
    if ([PFUser currentUser].username != NULL){
        NSString *sender = [userInfo valueForKey:@"send"];
        if( ![sender isEqualToString:[PFUser currentUser].username]){
            [PFPush handlePush:userInfo];
        }
    }else{
        [PFPush handlePush:userInfo];
    }
}



-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *newLocation = [locations lastObject];
    
    [ceo reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error == nil) {
            placemark = [placemarks lastObject];
            
            NSString *latitude, *longitude, *state, *country;
            longitude =placemark.locality;
            latitude = placemark.subLocality;
            state = placemark.administrativeArea;
            country = placemark.country;
            NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
            address =locatedAt;
            
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    }];
    
    // Turn off the location manager to save power.
    //[CLLocationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Cannot find the location.");
}

-(void)ShowHud
{
    self.hud = [MBProgressHUD showHUDAddedTo:self.window animated:YES];
    _hud.labelText = @"por favor espera..";
}
-(void)DismisHud
{
    [MBProgressHUD hideHUDForView:self.window animated:YES];
    self.hud = nil;
    
}

-(void)getImage:(NSString *)userName
{
    [self.imageDescriptionArray removeAllObjects];
    [self.imageLocationArray removeAllObjects];
    [self.imageDateTimeArray removeAllObjects];
    [self.imageDataArray removeAllObjects];
    [self.imageTitleArray removeAllObjects];
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"ImageData"];
    [query whereKey:@"ownername" equalTo:self.ownerName];
    //    [query whereKey:@"usertype" equalTo:[NSNumber numberWithInt:1]];
    [query orderByDescending:@"createdAt"];
    //query.limit = 20;
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
        if (results.count<1) {
            NSLog(@"No Photo Found...");
        }
        else {
            NSLog(@" Photos Found...");
            for (NSInteger i=0; i<[results count]; i++) {
                [self.imageDescriptionArray addObject:[[results objectAtIndex:i] objectForKey:@"description"]];
                [self.imageTitleArray addObject:[[results objectAtIndex:i] objectForKey:@"title"]];
                [self.imageLocationArray addObject:[[results objectAtIndex:i] objectForKey:@"location"]];
                [self.imageDateTimeArray addObject:[[results objectAtIndex:i] objectForKey:@"date"]];
                PFFile *imageFile = [[results objectAtIndex:i] objectForKey:@"image"];
                [self.imageDataArray addObject:imageFile.url];
                
            }
        }

        [[NSNotificationCenter defaultCenter]postNotificationName:@"ReportDataDownloadedFromServerNotification" object:nil];
        
    }];
}

-(void)checkUserName:(NSString *)userName : (NSString *)password :(NSNumber *)userType
{
    
    [PFUser logInWithUsernameInBackground:userName password:password
                                    block:^(PFUser *user, NSError *error) {
                                        
                                        

                                        if (user) {
                                            
                                            if(!self.didRegisterForPNonHeroku){
                                                [self registerForPNonHeroku];
                                            }
                                            
                                            NSLog(@"Successfully retrieved the object.");
                                            // All instances of TestClass will be notified
                                            self.userName = userName;
                                            self.password = password;
                                            self.ownerName =[user objectForKey:@"owner"];
                                            //self.userType = userType;
                                            
                                            [[NSUserDefaults standardUserDefaults]setValue:userName forKey:@"userName"];
                                             [[NSUserDefaults standardUserDefaults]setValue:self.ownerName forKey:@"ownerName"];
                                             [[NSUserDefaults standardUserDefaults]setValue:self.userType forKey:@"userType"];
                                            [[NSUserDefaults standardUserDefaults]synchronize];
                                            if([self.deviceID length]>5)
                                            {
                                                [self AddorUpdateDeviceID];
                                            }
                                            if([[user objectForKey:@"usertype"] intValue]==[userType intValue])
                                            {
                                                [[NSNotificationCenter defaultCenter]
                                                 postNotificationName:@"UserNameCheck_Notification"
                                                 object:self];
                                            }
                                            else{
                                                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops" message:@"You are not a right User!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                [alert show];
                                                [self DismisHud];
                                            }
                                            
                                        } else {
                                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops" message:@"Verifique su  contraseña " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                            [alert show];
                                            [self DismisHud];
                                            
                                        }
                                    }];
    
    
    
    
    //    PFQuery *query = [PFQuery queryWithClassName:@"User"];
    //    [query whereKey:@"username" equalTo:userName];
    //     [query whereKey:@"password" equalTo:password];
    //     [query whereKey:@"usertype" equalTo:userType];
    //
    //
    //    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
    //        if (!object) {
    //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops" message:@"Username and Password not Correct!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //            [alert show];
    //            [self DismisHud];
    //        } else {
    //            // The find succeeded.
    //            NSLog(@"Successfully retrieved the object.");
    //            // All instances of TestClass will be notified
    //            self.userName = userName;
    //             self.password = password;
    //            [[NSNotificationCenter defaultCenter]
    //             postNotificationName:@"UserNameCheck_Notification"
    //             object:self];
    //        }
    //    }];
}

- (void)uploadImage:(UIImage *)image Description:(NSString *)Des Title:(NSString*)title
{
    Reachability * networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable){
            [self saveImageLocaly:image descrioption:Des Title:title isUploaded:NO];
    }
    else{
        NSData *imageData = UIImageJPEGRepresentation(image, 0.05f);
        PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];
        PFObject *userPhoto = [PFObject objectWithClassName:@"ImageData"];
        [userPhoto setObject:imageFile forKey:@"image"];
        [userPhoto setObject:self.userName forKey:@"username"];
        [userPhoto setObject:self.ownerName forKey:@"ownername"];
        if(address==nil)
        {
            address = @" ";
        }
        [self sendPushToAllUserOfSameType];
        [userPhoto setObject:address forKey:@"location"];
        [userPhoto setObject:Des forKey:@"description"];
        [userPhoto setObject:title forKey:@"title"];
        [userPhoto setObject:self.userType forKey:@"usertype"];
        
        //---date --------
        
        //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        //[dateFormatter setDateFormat:@"dd/MM/yyyy  HH:mm"];
        //     [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        //   [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        //    NSLocale *curentLocale = [NSLocale currentLocale];
        //    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[curentLocale localeIdentifier]];
        //    [dateFormatter setLocale:locale];
        
        
        NSLog(@"%@",[NSDate date]);
        
        [userPhoto setObject:[NSDate date] forKey:@"date"];
        
        //    if([self.userType intValue]==2)
        //    {
        //        [self saveImageLocaly:image:Des];
        //    }
        
        [userPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                NSLog(@"NO Error");
                [self saveImageLocaly:image descrioption:Des Title:title isUploaded:YES];
            }
            else{
                // Log details of the failure
                NSLog(@"Error: %@ %@", error, [error userInfo]);
                [self saveImageLocaly:image descrioption:Des Title:title isUploaded:NO];
            }
        }];

    }
    
}

-(void)sendPushToAllUserOfSameType
{
    
    [self sendPushNotification:nil];
    /*
    NSLog(@"%@",self.ownerName);
    NSLog(@"%@",self.userType);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"owner = %@ AND userType = %@", self.ownerName,[NSString stringWithFormat:@"%d",1]];
    NSLog(@"%@",predicate);
    // PFQuery *query = [PFUser query];
    PFQuery *query = [PFQuery queryWithClassName:@"DeviceIDs" predicate:predicate];
    [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
        if (results.count<1) {
            NSLog(@"No Device Found...");
        }
        else {
            for (NSInteger i=0; i<[results count]; i++) {
                if(![[[results objectAtIndex:i] objectForKey:@"deviceID"] isEqualToString:self.deviceID])
                {
                    [self sendPushNotification:[[results objectAtIndex:i] objectForKey:@"deviceID"]];
                }
            }
            
        }
    }];*/
}

-(void)saveImageLocaly:(UIImage *)imagefile descrioption:(NSString *)Des Title:(NSString*)title isUploaded:(BOOL)isUploaded
{
    NSInteger imageNumber = [[NSUserDefaults standardUserDefaults]integerForKey:@"ImageNumber"];
    NSData *imageData = UIImageJPEGRepresentation(imagefile, 0);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"MyImageFolder"];
    
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    NSString *fullPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"imageName%ld.png",(long)imageNumber+1]]; //add our image to the path
    
    bool successI = [imageData writeToFile:fullPath atomically:YES];
    if (successI) {
        [[NSUserDefaults standardUserDefaults]setInteger:imageNumber+1 forKey:@"ImageNumber"];
        NSLog(@"Image saved correctly localy");
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:0];
        
        NSLog(@"%@",self.userName);
        [dict setObject:self.userType forKey:@"UserType"];
        [dict setObject:[NSNumber numberWithBool:isUploaded] forKey:@"IsUploaded"];
        [dict setObject:self.userName forKey:@"UserName"];
        
        [dict setObject:((address==nil) ? @"":address) forKey:@"Location"];
        [dict setObject:Des forKey:@"Description"];
        [dict setObject:[NSDate date] forKey:@"DateTime"];
        [dict setObject:title forKey:@"Title"];
        [dict setObject:[NSString stringWithFormat:@"imageName%ld.png",(long)imageNumber+1] forKey:@"ImageName"];
        NSMutableArray *alertArrayDefualt = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ImageArray"]];
        [alertArrayDefualt addObject:dict];
        [[NSUserDefaults standardUserDefaults] setObject:alertArrayDefualt forKey:@"ImageArray"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    else
        NSLog(@"Error while saving Image");
}


-(void)testPost{
    NSString *post = @"username=xcode&password=123456";
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://equidad.herokuapp.com/parse/users"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"CpkjkPtVi84QARtGotGpJ99Q1XnJgJsvjfEfsqsK" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:@"7usr72qbfirzlvD7SpsHba63txMm9xKcC9lzVVPR" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:nil error:nil];
    
    NSLog(@"Response: %@",[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
}

-(void)getSavedImagesData{
    
    
    NSMutableArray *imagesData = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ImageArray"]];
    for (NSDictionary *imageData in imagesData){
        NSLog(@"ImageData:%@",imageData);
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    self.userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"userName"];
    self.ownerName = [[NSUserDefaults standardUserDefaults] valueForKey:@"ownerName"];
    if(self.userName != nil && self.ownerName!=nil){
        self.unloadedImages = [self getUnloadedImagesOfUser];
        [self uploadUnloadedImages];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// Send Push Notification to recipients

-(void)sendPushNotification:(NSString *)deviceID
{
   /* PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"channels" equalTo:deviceID];
    //[pushQuery whereKey:@"deviceToken" equalTo:@"8979a5e446114e11888386205d2b4e1ca23302b75d5ffd1f67c5cab578457613"];
    PFPush *push = [[PFPush alloc] init];
    [push setQuery:pushQuery];
    //     [push setMessage:[NSString stringWithFormat: @"New Message from @!"]];
    [push setMessage:[NSString stringWithFormat: @"Hay una nueva foto de %@!",  [PFUser currentUser].username]];
    [push sendPushInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded)
        {
            NSLog(@"push sent sucessfully");
        }
        else{
            NSLog(@"not push sent sucessfully");
        }
    }];*/
    
    //deviceID = @"userc9910ef213f37516f2c95253274ada28680d496c2c098c96f24ca0141f565860";
    NSString *sender =  [PFUser currentUser].username;
    NSString *message = [NSString stringWithFormat: @"Hay una nueva foto de %@!",  [PFUser currentUser].username];
    NSMutableDictionary * pushData = [[NSMutableDictionary alloc]init];
    // [pushData setObject:deviceIDs forKey:@"channels"];
    [pushData setObject:self.ownerName forKey:@"channels"];
    [pushData setObject:message forKey:@"message"];
    [pushData setObject:sender forKey:@"sender"];
    
    /*NSString *message = [NSString stringWithFormat: @"Hay una nueva foto de %@!",  [PFUser currentUser].username];
    NSMutableDictionary * pushData = [[NSMutableDictionary alloc]init];
    [pushData setObject:deviceID forKey:@"channels"];
    [pushData setObject:message forKey:@"message"];
    */
    /* NSError *error = nil;
     [PFCloud callFunction:@"SendPush" withParameters:pushData error:&error];
     if(error != nil){
     NSLog(@"FailedToSendPN:%@",error.description);
     }else{
     NSLog(@"Push Send");
     }*/
    
    //  [PFCloud callFunctionInBackground:@"SendPush" withParameters:pushData];

   
    [PFCloud callFunctionInBackground:@"SendPush" withParameters:pushData block:^(id  _Nullable object, NSError * _Nullable error) {
        if(error != nil){
            NSLog(@"FailedToSendPN:%@",error.description);
        }else{
            NSLog(@"Push Send successfully");
        }
    }];
    
//    [PFCloud callFunctionInBackground:@"hello" withParameters:pushData block:^(id  _Nullable object, NSError * _Nullable error) {
//        if(error != nil){
//            NSLog(@"FailedToSendPN:%@",error.description);
//        }else{
//            NSLog(@"Push Send successfully");
//        }
//    }];
    
}

-(void)AddorUpdateDeviceID
{
    PFQuery *query = [PFQuery queryWithClassName:@"DeviceIDs"];
    [query whereKey:@"deviceID" equalTo:self.deviceID];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!object) {
            [self AddDatatoDeviceIDTable];
        } else {
            [object setObject:[NSString stringWithFormat:@"%d",[self.userType intValue]] forKey:@"userType"];  //fix usertype to 1;
            [object setObject:self.ownerName forKey:@"owner"];
            
            [object setObject:self.deviceID forKey:@"deviceID"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    NSLog(@"NO Error");
                }
                else{
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
            
        }
    }];
}

-(void)AddDatatoDeviceIDTable
{
    NSLog(@"%@",self.ownerName);
    NSLog(@"%@",self.userType);
    NSLog(@"%@",self.deviceID);
    
    PFObject *userPhoto = [PFObject objectWithClassName:@"DeviceIDs"];
    [userPhoto setObject:[NSString stringWithFormat:@"%d",[self.userType intValue]] forKey:@"userType"];
    [userPhoto setObject:self.ownerName forKey:@"owner"];
    
    [userPhoto setObject:self.deviceID forKey:@"deviceID"];
    
    
    [userPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSLog(@"NO Error");
        }
        else{
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
}

//MARK: UPloading Unloaded images

-(void)uploadUnloadedImages{
    
    if(unloadedImages.count <=0){
        return;
    }
    int imageIndex=0;
    NSDictionary *imageDict = [unloadedImages objectAtIndex:imageIndex];
    NSString *imageName = [NSString stringWithFormat:@"MyImageFolder/%@",[imageDict valueForKey:@"ImageName"]];
    NSString *filepath=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:imageName];
    
    UIImage *image = [UIImage imageWithContentsOfFile:filepath];
    NSString *userType = [imageDict objectForKey:@"UserType"];
    //NSString *userName = [imageDict objectForKey:@"UserName"];
    NSString *location = [imageDict objectForKey:@"Location"];
    NSString *description = [imageDict objectForKey:@"Description"];
    NSString *title = [imageDict objectForKey:@"Title"];
    NSString *dateTime = [imageDict objectForKey:@"DateTime"];
    long int imageArrayIndexOfEntry = [[imageDict objectForKey:@"imageArrayIndex"] integerValue];
    //NSString *imageName = [imageDict objectForKey:@"ImageName"];
    
    
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.05f);
    PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];
    PFObject *userPhoto = [PFObject objectWithClassName:@"ImageData"];
    [userPhoto setObject:imageFile forKey:@"image"];
    [userPhoto setObject:self.userName forKey:@"username"];
    [userPhoto setObject:self.ownerName forKey:@"ownername"];
    [userPhoto setObject:location forKey:@"location"];
    [userPhoto setObject:description forKey:@"description"];
    [userPhoto setObject:title forKey:@"title"];
    [userPhoto setObject:userType forKey:@"usertype"];
    [userPhoto setObject:dateTime forKey:@"date"];
    
    [userPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSLog(@"NO Error");
            [self markStatusAsUploadInUserDefaultImagesArrayforEntryAtIndex:imageArrayIndexOfEntry];
            [unloadedImages removeObjectAtIndex:0];
            [self uploadUnloadedImages];
        }
        else{
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            
        }
    }];
    
}

-(NSMutableArray*)getUnloadedImagesOfUser{
    
    NSMutableArray *unloadedEntries = [[NSMutableArray alloc]init];
    NSMutableArray *totalEntries = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ImageArray"]];
    int index = 0;
    for (NSDictionary *entry in totalEntries){
        
        NSMutableDictionary *dict = [entry mutableCopy];
        id isUploadedObj = [entry valueForKey:@"IsUploaded"];
        BOOL isUploaded = NO;
        if(isUploadedObj != nil){
            isUploaded = [isUploadedObj boolValue];
        }
        else{
            isUploaded = YES;  //for all the images previously uploaded(If Key not exist)
        }
        
        
         NSLog(@"UserName:%@",self.userName);
        if( [[entry valueForKey:@"UserName"] isEqualToString:self.userName] && (!isUploaded))
        {
            [dict setObject:[NSNumber numberWithInt:index] forKey:@"imageArrayIndex"];
            [unloadedEntries addObject:dict];
        }
        
        index++;
        
    }
    return unloadedEntries ;
    
}

-(void)markStatusAsUploadInUserDefaultImagesArrayforEntryAtIndex:(long int)index{
    NSMutableArray *totalEntries = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ImageArray"]];
    NSMutableDictionary *dict = [[totalEntries objectAtIndex:index]mutableCopy];
    [dict setObject:[NSNumber numberWithBool:YES] forKey:@"IsUploaded"];
    [totalEntries replaceObjectAtIndex:index withObject:dict];
    [[NSUserDefaults standardUserDefaults]setObject:totalEntries forKey:@"ImageArray"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}


@end
