 //
//  TakePictureViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/31/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import "TakePictureViewController.h"
#import "WGViewController.h"
#import "WebViewController.h"
#import <Parse/Parse.h>
#import "SendPicturesViewController.h"
#import "PopUpViewController.h"
#import "WGAppDelegate.h"

@interface TakePictureViewController ()

@end

@implementation TakePictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isSendPressed = NO;
   // [titleField becomeFirstResponder];
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)goToHomePage:(id)sender

{
     [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)goToWebview:(id)sender
{
    WebViewController *wb=[[WebViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:wb animated:YES];
    
}
-(IBAction)takePicture: (id)sender
{
    [self openImagePickerWithSourceType: UIImagePickerControllerSourceTypeCamera];
    
}
- (void)openImagePickerWithSourceType: (UIImagePickerControllerSourceType)sourceType
{
    if ( ![UIImagePickerController isSourceTypeAvailable: sourceType] ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString( @"Error", @"" )
                                                        message: NSLocalizedString( @"We are sorry, but this functionality is not available at your device.", @"No camera error" )
                                                       delegate: nil
                                              cancelButtonTitle: NSLocalizedString( @"Dismiss", @"")
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = sourceType;
    self.isCameraShown = YES;
    
    
    //Sun - iPad support
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.popover = [[UIPopoverController alloc] initWithContentViewController:(UIViewController *)picker];
        CGRect takePhotoRect;
        takePhotoRect.origin = self.view.frame.origin;
        takePhotoRect.size.width = 1;
        takePhotoRect.size.height = 1;
        [self.popover setPopoverContentSize:CGSizeMake(320.0, 216.0)];
        
        [self.popover presentPopoverFromRect:takePhotoRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else{
        [self presentViewController:picker animated:YES completion:NULL ];
    }
    
    
}


- (IBAction)actionUseCameraRoll:(id)sender
{
    
//    if (![self isChecked])
//        return;
    
    UIImagePickerController *ipc_Album = [[UIImagePickerController alloc] init];
    ipc_Album.delegate = self;
    ipc_Album.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //    ipc_Album.allowsEditing = YES;
    //    ipc_Album.canBecomeFirstResponder = NO;
    [self presentViewController:ipc_Album animated:YES completion:nil];
    
   // [self working];
}

- (void)imagePickerController:(UIImagePickerController *)picker  didFinishPickingMediaWithInfo:(NSDictionary *)info {
    showImage.contentMode = UIViewContentModeScaleAspectFill;
    [showImage setClipsToBounds:YES];
    showImage.image=[info objectForKey:UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES];
//    [self changeCancelToAnythingUWantOf:viewController andReplacementString:@"Replacement"];
//}

//- (void)changeCancelToAnythingUWantOf:(UIViewController*)vc andReplacementString:(NSString*)title {
//    
//    for(UIView *subView in vc.view.subviews) {
//        if ([subView isKindOfClass:[UIButton class]]) {
//            UIButton *btn = (UIButton *)subView;
//            if([btn.titleLabel.text isEqualToString:@"Retake"]) {
//                [btn setTitle:title forState:UIControlStateNormal];
//                break;
//            }
//        }
//        
//        if (subView.subviews) {
//            for (UIView *subSubView in subView.subviews) {
//                if ([subSubView isKindOfClass:[UIButton class]]) {
//                    UIButton *btn = (UIButton *)subSubView;
//                    if([btn.titleLabel.text isEqualToString:@"Use Photo"]) {
//                        [btn setTitle:title forState:UIControlStateNormal];
//                        break;
//                    }
//                }
//                
//                if (subSubView.subviews) {
//                    for (UIView *subSubSubView in subSubView.subviews) {
//                        if ([subSubSubView isKindOfClass:[UIButton class]]) {
//                            UIButton *btn = (UIButton *)subSubSubView;
//                            if([btn.titleLabel.text isEqualToString:@"Cancel"]) {
//                                [btn setTitle:title forState:UIControlStateNormal];
//                                break;
//                            } 
//                        }
//                    }
//                }
//            }
//        }
//    }
//}

-(IBAction)mailButtonClicked:(id)sender
{
    
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:@"Share this app"];
    [mailComposer setMessageBody:@"Share this app with your loved ones" isHTML:NO];
    [self presentViewController:mailComposer animated:YES completion:nil];
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    //Add an alert in case of failure
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)sendPicture:(id)sender
{
    [tf1 resignFirstResponder];
    [titleField resignFirstResponder];
    
    if(!isSendPressed){
        isSendPressed = YES;
        if(showImage.image!=nil && tf1.text.length>=1)
        {
            WGAppDelegate *app = (WGAppDelegate *)[UIApplication sharedApplication].delegate;
            NSString *title = titleField.text;
            NSString *description = tf1.text;
            [app uploadImage:showImage.image Description:description Title:title];
            wb=[[PopUpViewController alloc]initWithNibName:nil bundle:nil];
            wb.view.center = self.view.center;
            [self.view addSubview:wb.view];
            
        }
        else{
            isSendPressed = NO;
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops!!" message:@"Por favor escriba la descripción para poder enviar..." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }
   
 }

- (void)textViewDidEndEditing:(UITextView *)textView{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}


- (void)textViewDidBeginEditing:(UITextView *)textView {
   // editImage.hidden = YES;
    NSLog(@"textViewDidBeginEditing:");
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-170, self.view.frame.size.width, self.view.frame.size.height)];
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];

}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


- (void)textViewDidChangeSelection:(UITextView *)textView
{
    tf1.text = textView.text;
    NSArray* components = [textView.text componentsSeparatedByString:@"\n"];
    if ([components count] > 0) {
       // NSString* commandText = [components lastObject];
        // and optionally clear the text view and hide the keyboard...
        //[textView resignFirstResponder];
    }

}

-(IBAction)returnKeyPressed:(UITextField*)sender{
    [sender resignFirstResponder];
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:  UIViewAnimationCurveEaseOut
                     animations:^
     {
         [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+150, self.view.frame.size.width, self.view.frame.size.height)];
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}

-(IBAction)titleEditingStarted:(UITextField*)sender{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:  UIViewAnimationCurveEaseOut
                     animations:^
     {
         [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-150, self.view.frame.size.width, self.view.frame.size.height)];
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
