//
//  SeeOneByOneViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 1/3/15.
//  Copyright (c) 2015 Apnovator-Prakshi. All rights reserved.
//

#import "SeeOneByOneViewController.h"
#import "WGViewController.h"
#import "WebViewController.h"
#import "CustomCell.h"
#import "ImageDisplayViewController.h"
#import "UIViewAnimationExplose.h"

@interface SeeOneByOneViewController ()

@end
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
@implementation SeeOneByOneViewController


-(void)UpdateTable{
    [app  DismisHud];
    [activityIndicatorView stopAnimating];
    [myTableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateTable) name:@"ReportDataDownloadedFromServerNotification" object:nil];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
    
}


- (void)viewDidLoad {
    counter = 0;
    [super viewDidLoad];
    popUpView.hidden = YES;
    dateFormatter = [[NSDateFormatter alloc]init];
    self.allViews = [[NSMutableArray alloc]init];
    app = (WGAppDelegate *)[UIApplication sharedApplication].delegate;
  //  NSLog(@"Array location %@",app.imageDescriptionArray);
    // NSLog(@"Array location %@",app.imageLocationArray);
   //  NSLog(@"Array location %@",app.imageDateTimeArray);
     NSLog(@"Array location %@",app.imageDataArray);
    
//    [app reverseArray:app.imageDescriptionArray];
//     [app reverseArray:app.imageLocationArray];
//     [app reverseArray:app.imageDataArray];
//     [app reverseArray:app.imageDateTimeArray];
    
    
    
    [myTableView registerNib:[UINib nibWithNibName:@"CustomCell"bundle:nil] forCellReuseIdentifier:@"Cell"];
    [myTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)goToHomePage:(id)sender

{
   // [self.navigationController popToRootViewControllerAnimated:YES];
     [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)goToWebview:(id)sender
{
    WebViewController *wb=[[WebViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:wb animated:YES];
}

-(IBAction)NextBtnClick:(id)sender
{
    textViewDesc.text =[app.imageDescriptionArray objectAtIndex:counter];
    lblLocation.text = [app.imageLocationArray objectAtIndex:counter];
    lblTime.text =[app.imageDateTimeArray objectAtIndex:counter];
    counter++;
    
    
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 446;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // We are looking for cells with the Cell identifier
    // We should reuse unused cells if there are any
    static NSString *cellIdentifier = @"Cell";
    CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // If there is no cell to reuse, create a new one
    if(cell == nil)
    {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
       
    }
     [cell.plusButton addTarget:self action:@selector(plusButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    // Configure the cell before it is displayed...

    cell.plusButton.tag = indexPath.row;

  cell.valueDescription.text =[app.imageDescriptionArray objectAtIndex:indexPath.row];
   cell.labelLocation.text = [app.imageLocationArray objectAtIndex:indexPath.row];
    
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    NSString * dateFormatted = [dateFormatter stringFromDate:[app.imageDateTimeArray objectAtIndex:indexPath.row]];
    cell.labelDate.text =dateFormatted;
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString * dateFormatted1 = [dateFormatter stringFromDate:[app.imageDateTimeArray objectAtIndex:indexPath.row]];
    cell.labelTime.text =dateFormatted1;
    NSString *title  = [app.imageTitleArray objectAtIndex:indexPath.row];
    cell.title.text = title;
    cell.ShowimageView.contentMode = UIViewContentModeScaleAspectFill;
    [cell.ShowimageView setClipsToBounds:YES];
    [cell.ShowimageView setImageWithURL:[NSURL URLWithString:[app.imageDataArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Enviar fotos_Iphone5_Botonreloj.png"] options:SDWebImageRefreshCached|SDWebImageProgressiveDownload];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    imageURL =[NSURL URLWithString:[app.imageDataArray objectAtIndex:indexPath.row]];
    [self isplayBigImage];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [app.imageDataArray count];
}

 
-(IBAction)NamesClick_Tapped:(id)sender
{
//    [myTableView scrollRectToVisible:CGRectMake(myTableView.frame.origin.x, myTableView.frame.origin.y, myTableView.frame.size.width, myTableView.frame.size.height+800) animated:YES];
//  CGFloat oldTableViewHeight = myTableView.contentSize.height;
     [myTableView setContentOffset:CGPointMake(0,myTableView.contentOffset.y+446) animated:YES];
    // Reload your table view with your new messages
    
    // Put your scroll position to where it was before
  //  CGFloat newTableViewHeight = myTableView.contentSize.height;
  //  myTableView.contentOffset = CGPointMake(0, newTableViewHeight - oldTableViewHeight);
}

//-------------------------------------------------------------------------------------------------------------------------------------

-(void)isplayBigImage
{
    if([self iPhone6PlusDevice])
    {
    imageVC=[[ImageDisplayViewController alloc]initWithNibName:@"ImageDisplayViewController-HD" bundle:nil];
         imageVC.isImageShow = NO;
    }
    else{
        if([[UIScreen mainScreen] bounds].size.height == 667.0)
        {
         imageVC=[[ImageDisplayViewController alloc]initWithNibName:@"ImageDisplayViewController-HD" bundle:nil];
             imageVC.isImageShow = NO;
        }
        else{
         imageVC=[[ImageDisplayViewController alloc]initWithNibName:@"ImageDisplayViewController" bundle:nil];
             imageVC.isImageShow = NO;
        }
     
    }
    imageVC.imageURL = imageURL;
    imageVC.view.center = self.view.center;
    [self.view addSubview:imageVC.view];

}

-(BOOL)iPhone6PlusDevice{
    if ([UIScreen mainScreen].scale > 2.9) return YES;   // Scale is only 3 when not in scaled mode for iPhone 6 Plus
    return NO;
}


-(void)plusButtonClick:(UIButton*)button{
    
   // int rowIndex = button.tag;
    UIView *contentView = button.superview;
    CustomCell *cell = (CustomCell*)contentView.superview;
    
    
    popUpTitle.text = cell.title.text;
    popUpDescription.text = cell.valueDescription.text;
    popUpImageView.image = cell.ShowimageView.image;
    popUpView.hidden = false;
    
}

-(void)dismissPopUp:(UIButton*)sender{
    popUpView.hidden = true;
}


@end
