//
//  ImageDisplayViewController.m
//  WayGroup
//
//  Created by Amar on 05/02/15.
//  Copyright (c) 2015 Apnovator-Prakshi. All rights reserved.
//

#import "ImageDisplayViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface ImageDisplayViewController ()

@end

@implementation ImageDisplayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // NSLog(@"%@",self.imageURL);
    if(!self.isImageShow)
    {
 [self.displayImage setImageWithURL:self.imageURL placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached|SDWebImageProgressiveDownload];
    }
    else{
         UIImage *testImage = [UIImage imageWithContentsOfFile:self.imageData];
    self.displayImage.image =testImage;
    }
    
    // Do any additional setup after loading the view from its nib.
    // viewWin.center = self.view.center;
    [self.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)CloseBtnClick:(id)sender
{
    [self.view removeFromSuperview];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
