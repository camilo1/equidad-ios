//
//  ImageDisplayViewController.h
//  WayGroup
//
//  Created by Amar on 05/02/15.
//  Copyright (c) 2015 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageDisplayViewController : UIViewController

{
    IBOutlet UIImageView *displayImage;

}

@property (nonatomic, strong) IBOutlet UIImageView *displayImage;
@property (nonatomic, strong) NSString *imageData;
@property (nonatomic) bool  isImageShow;
@property (nonatomic, assign) NSURL  *imageURL;
-(IBAction)CloseBtnClick:(id)sender;
@end
