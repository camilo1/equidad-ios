//
//  SeeOneByOneViewController.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 1/3/15.
//  Copyright (c) 2015 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGAppDelegate.h"
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "AnimationExploseContainer.h"
#import "ImageDisplayViewController.h"
@interface SeeOneByOneViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSDateFormatter * dateFormatter;
    IBOutlet UITableView *myTableView;
    WGAppDelegate *app;
    int counter;
    IBOutlet UIImageView *imageShow;
    IBOutlet UILabel *titleLabel;
    IBOutlet UITextView * textViewDesc;
    IBOutlet UILabel * lblTime;
    IBOutlet UILabel * lblLocation;
    ImageDisplayViewController *imageVC;
    NSURL *imageURL;

    IBOutlet UIView *popUpView;
    IBOutlet UILabel *popUpTitle;
    IBOutlet UITextView *popUpDescription;
    IBOutlet UIImageView *popUpImageView;
    IBOutlet UIActivityIndicatorView *activityIndicatorView;

}
@property (nonatomic, retain) NSMutableArray *allViews;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIProgressView *progressBar;
@property (nonatomic, retain) AnimationExploseContainer *exploseContainer;
-(IBAction)NamesClick_Tapped:(id)sender;
-(IBAction)goToHomePage:(id)sender;
-(IBAction)dismissPopUp:(UIButton*)sender;
@end
