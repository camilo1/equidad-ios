//
//  UserOneViewController.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/30/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGAppDelegate.h"
@interface UserOneViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    IBOutlet UITextField *tf1;
    IBOutlet UITextField *tf2;
    IBOutlet UIScrollView *scrollView;
    UITextField *currentTextField;
    BOOL keyboardIsShown;
    IBOutlet UIButton *btnclick;
    IBOutlet UILabel *lb;
    WGAppDelegate *app;
}
-(IBAction)enter:(id)sender;
-(IBAction)userOneReport: (id)sender;
@property (nonatomic, assign) BOOL isCameraShown;
@property (retain, nonatomic) UIPopoverController *popover;

@end
